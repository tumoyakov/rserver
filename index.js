const express = require("express"); // подключается библиотека сервера
const app = express(); // создается экземпляр сервера
const bodyParser = require("body-parser"); // подключается парсер запроса
const R = require("r-script"); // подключается библиотека для R скриптов
const fs = require("fs"); // подключается библиотека для работы с файловой системой

if (process.stdout._handle) process.stdout._handle.setBlocking(true); // не помню что такое, что то с блокирующим потоком вывода

/**
 * Логгер, тут служит для вывода на консоль принятого запроса (его метод и путь)
 * @param {*} req - объект запроса с клиента, здесь хранятся все данные, такие как путь,
 * параметры добавленные в путь типа (localhost/list/2 - 2 -это параметр - номер списка),
 * либо данные в теле запроса (req.body)
 * @param {*} res - объект ответа, сюда передается статус ответа (к примеру 404 - не найдено), и данные (res.send({data}))
 * @param {*} next - нужен для создания цепочки вызовов, чтобы для одного пути выполнилось несколько функций.
 */
function logger(req, res, next) {
  console.log("%s %s", req.method, req.url);
  next();
}

/**
 * allowCrossDomain - нужна чтобы позволить с каких доменов,
 * с какими заголовками и по каким методам сервер будет принимать запросы
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
var allowCrossDomain = function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
  res.header("Access-Control-Allow-Headers", "*");
  next();
};

//задание цепочки вызовов функций
app.use(logger);
app.use(bodyParser.json()); // парсим запрос в js объект
app.use(bodyParser.urlencoded({ extended: true }));
app.use(allowCrossDomain);

/**
 * обработка запроса пришедшему по пути '/'
 * @param script - скрипт переданный с клиента в виде строки.
 * Так как бибилиотека r-script принимает путь скрипта, а не сам текст скрипта, то нужно сперва создать файл из строки
 */
app.post("/", function (req, res) {
  const script = req.body.script;

  const scriptPath = `${process.cwd()}\\file\\${new Date().getTime()}.R`; // задаем имя файла - он будет создан в директории files c текущей датой как именем
  fs.writeFileSync(scriptPath, script); // файл создается

  new R(scriptPath).data("hello world", 20).call(function (err, d) {
    //.data содержит эти параметры просто чтобы работал тестовый скрипт, их можно удалить
    if (err) {
      //обработка в случае возникновения ошибки
      fs.unlink(scriptPath, () => {}); //удаляем файл скрипта
      return res.status(500).send(err); //отправляем на клиент ответ со статусом 500 и полученной ошибкой
    }
    fs.unlink(scriptPath, () => {}); // в случае корректного завершения также удаляем файл
    return res.status(200).send(d); // и отправляем ответ со статусом 200 (типа все окей) и полученными данными
  });
});

app.listen(8000); //запуск сервера на 8000 порту, можно изменить на любой другой.
